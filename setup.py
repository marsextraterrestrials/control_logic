from setuptools import setup

setup(name='control_logic',
      version='0.1.0',
      description='A collection of (hopefully) reusable utilities for controlling simple Raspberry Pi-based robots from Python',
      author='Pal Denes',
      author_email='pdenes@codingafter9.com',
      url='https://gitlab.com/marsextraterrestrials/control_logic',
      packages=['control_logic'],
      install_requires=['numpy'],
      classifiers=[
          'Programming Language :: Python :: 3',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Operating System :: OS Independent',
      ],
      python_requires='>=3.5')
