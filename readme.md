# control_logic

A collection of (hopefully) reusable utilities for controlling simple Raspberry Pi-based robots from Python.

These are all about simple utility functions and control logic algorithms and don't depend on any specific hardware.

