MOTOR_STATE_NORMAL = 0
MOTOR_STATE_DEADBAND = 1
MOTOR_STATE_WAIT = 2

MOTOR_WAIT_SECS = 0.5

class SafeMotor:
    """
    "Safe" motor control: avoids current spikes when polarity changes suddenly by delaying changes that reverse direction.
    Also constraints the speed (power) setting to [-10..10] and implements a dead band around 0.
    Doesn't depend on any specific hardware, only implements "abstract" logic: takes an input value and produces a "safe" output.
    """
    def __init__(self, deadband = 0):
        self.last_t = 0
        self.out = 0
        self.out_before_wait = 0
        self.wait = 0
        self.state = MOTOR_STATE_NORMAL
        self.deadband = deadband
    def update(self, t, inp):
        """
        Update motor state and return output to be passed on to the actual motor driver. This should be called periodically.
        :param t: current timestamp
        :inp: input (-10..10)
        :return: "safe" motor output based on input
        """
        dt = t - self.last_t
        self.last_t = t

        if self.wait > 0:
            if inp > 0 and self.out_before_wait > 0 or inp < 0 and self.out_before_wait < 0:
                # was waiting but input went back to the side it was before: allow it
                self.wait = 0
                self.state = MOTOR_STATE_NORMAL
            else:
                # still waiting?
                self.wait = max(0, self.wait - dt)
                if self.wait == 0:
                    self.state = MOTOR_STATE_NORMAL
        elif inp >= 0 and self.out < 0 or inp <= 0 and self.out > 0:
            # trying to reverse polarity: set to 0 instead and wait
            self.state = MOTOR_STATE_WAIT
            self.out_before_wait = self.out
            self.wait = MOTOR_WAIT_SECS

        # check if we're still waiting (or stopped waiting above) - if not, try to follow inp:
        if self.wait > 0:
            self.out = 0
        else:
            self.out = inp
            if inp != 0 and inp < self.deadband and inp > -self.deadband:
                self.state = MOTOR_STATE_DEADBAND
                self.out = 0
            else:
                self.state = MOTOR_STATE_NORMAL
                self.out = inp
        
        return self.out
