import numpy as np

def pol2cart(v):
    """
    Convert polar coordinates to Cartesian
    :param v: (length, angle in radians) tuple
    :returns: (x, y) tuple
    """
    (rho, phi) = v
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)

def cart2pol(p):
    """
    Convert Cartesian coordinates to polar
    :param p: (x, y) tuple
    :returns: (length, angle in radians) tuple
    """
    (x, y) = p
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return (rho, phi)

def cart2len(p):
    """
    Return the length of a vector defined by its (x,y) endpoint
    (return the length part of polar coordinates converted from Cartesian)
    """
    (x, y) = p
    return np.sqrt(x**2 + y**2)

def cart2angle(p):
    """
    Return the angle (in radians) of a vector defined by its (x,y) endpoint
    (return the angle part of polar coordinates converted from Cartesian)
    """
    (x, y) = p
    return np.arctan2(y, x)

def square2circle(x, y):
    """
    Map coordinates (in [-1..1]) on square to a circle. Can be used to map (x,y) values between [-1,1] coming from a game controller joystick
    to a circle with radius = 1
    (see https://stackoverflow.com/questions/1621831/how-can-i-convert-coordinates-on-a-square-to-coordinates-on-a-circle?noredirect=1&lq=1)
    :param x: x coordinate in [-1,1]
    :param y: y coordinate in [-1,1]
    :returns: (x, y) tuple with coordinates inside the unit circle
    """
    return x * np.sqrt(1.0 - y**2 / 2.0), y * np.sqrt(1.0 - x**2 / 2.0)

def speed_dir_from_joystick(x, y):
    """
    Converts (x,y) coordinates from a game controller joystick (already mapped to a circle) to (speed, dir):
    speed is in [-10,10] (proportional to the length of the (x,y) vector, negative when y<0)
    dir is in [-90,90] (0 when y=1)
    :param x: x coordinate in [-1,1]
    :param y: y coordinate in [-1,1]
    :returns: (speed, dir) tuple
    """
    if x == 0 and y == 0:
        return (0, 0)
    else:
        (speed, dir) = cart2pol((x, -y))
        dir = np.rad2deg(dir) + 90
        if dir > 90:
            dir = dir - 180
            speed = -speed
        elif dir < -90:
            speed = -speed
        return (int(round(speed * 10)), int(round(dir)))

def left_right_motor_speed(speed, dir):
    """
    Convert speed and turn direction input to left-right motor speed.
    Examples:
    (10, 0) -> (10, 10): drive forward, full speed
    (10, 15) -> (10, 7): drive as fast as possible while turning slightly right (right motor goes slower)
    (10, -45) -> (0, 10): turn left by stopping left motor and running right motor full speed
    (5, 90) -> (5, -5): turn right in-place at half speed by running motors in opposite direction
    :param speed: speed in [-10..10]
    :param dir: turn direction in [-90..90]
    :return: (left, right) in [-10..10]
    """
    left = float(speed)
    right = float(speed)
    range = speed * 2.0
    diff = (float(dir)/0.9) * (range/100.0)
    if dir < 0:
        left = left + diff
    else:
        right = right - diff
    return (round(left), round(right))
    
def sum_vectors(vectors):
    """
    Return the sum of vectors (in polar coordinates)
    """
    #TODO check this, maybe a faster implementation is possible: https://math.stackexchange.com/questions/1365622/adding-two-polar-vectors
    return cart2pol(np.sum([np.array(pol2cart(v)) for v in vectors], 0))

